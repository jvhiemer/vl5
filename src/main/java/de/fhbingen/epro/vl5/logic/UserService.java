/**
 * 
 */
package de.fhbingen.epro.vl5.logic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhbingen.epro.vl5.spring.sql.repositories.user.UserRepository;
import de.fhbingen.epro.vl5.spring.sql.user.User;

/**
 * @author johanneshiemer
 *
 */
@Component
public class UserService {

	@Autowired
	private UserRepository userRepository;
	
	public void saveUser(User user) {
		userRepository.save(user);
	}
	
	public void findByFirstname(String firstname) {
		userRepository.findByFirstname(firstname);
	}
	
	public void listAllUser() {
		userRepository.findAll();
	}
}
