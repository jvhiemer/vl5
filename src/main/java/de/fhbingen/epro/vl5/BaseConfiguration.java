package de.fhbingen.epro.vl5;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

// "de.fhbingen.epro.vl5.spring.nosql.config"
@Configuration
@ComponentScan(basePackages = { "de.fhbingen.epro.vl5.spring.sql.config",
		"de.fhbingen.epro.vl5.logic"
})
public class BaseConfiguration {
}
