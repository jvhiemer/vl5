/**
 * 
 */
package de.fhbingen.epro.vl5.spring.sql.repositories.user;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import de.fhbingen.epro.vl5.spring.sql.user.User;

/**
 * @author johanneshiemer
 *
 */
public interface UserRepository extends PagingAndSortingRepository<User, Long> {
	
	List<User> findByFirstname(@Param("firstname") String firstname);

}
