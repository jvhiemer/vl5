/**
 * 
 */
package de.fhbingen.epro.vl5.spring.nosql.repositories.user;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import de.fhbingen.epro.vl5.spring.nosql.user.NoSqlUser;

/**
 * @author johanneshiemer
 *
 */
public interface NoSqlUserRepository extends CrudRepository<NoSqlUser, ObjectId> {
	
	List<NoSqlUser> findByName(@Param("name") String name);

}
