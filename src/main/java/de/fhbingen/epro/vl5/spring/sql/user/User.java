/**
 * 
 */
package de.fhbingen.epro.vl5.spring.sql.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import de.fhbingen.epro.vl5.spring.sql.AbstractEntity;

/**
 * @author johanneshiemer
 *
 */
@Entity
@Table(name = "\"user\"")
public class User extends AbstractEntity {

	@Column
	private String name;
	
	@Column
	private String firstname;
	
	public User() {
		super();
	}

	public User(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	
}
