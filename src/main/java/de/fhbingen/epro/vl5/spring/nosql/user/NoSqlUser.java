/**
 * 
 */
package de.fhbingen.epro.vl5.spring.nosql.user;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import de.fhbingen.epro.vl5.spring.nosql.AbstractDocument;

/**
 * @author Johannes Hiemer.
 *
 */
@Document
public class NoSqlUser extends AbstractDocument {

	private ObjectId id;
	
	private String name;

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
