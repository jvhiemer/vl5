/**
 * 
 */
package de.fhbingen.epro.vl5.spring.nosql;

import javax.persistence.Id;

import org.bson.types.ObjectId;

/**
 * @author Johannes Hiemer.
 *
 */
public abstract class AbstractDocument {

	@Id
	private ObjectId id;

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if ((this.id == null) || (obj == null)
				|| !(this.getClass().equals(obj.getClass()))) {
			return false;
		}

		AbstractDocument that = (AbstractDocument) obj;

		return this.id.equals(that.getId());
	}

	public ObjectId getId() {
		return this.id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return this.id == null ? 0 : this.id.hashCode();
	}

}
