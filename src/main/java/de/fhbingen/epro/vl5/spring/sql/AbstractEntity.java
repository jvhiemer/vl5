/**
 * 
 */
package de.fhbingen.epro.vl5.spring.sql;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.SequenceGenerator;

/**
 * @author johanneshiemer
 *
 */
@MappedSuperclass
public class AbstractEntity {
	
	@Id
	@SequenceGenerator(name = "seqGenerator", sequenceName = "DICTIONARY_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqGenerator")
	private Long id;

	/**
	 * Returns the identifier of the entity.
	 * 
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

}
